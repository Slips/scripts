# Scripts.

Directory:

util/

	edp - disables HDMI output, enables eDP output.

	hdmi - disables eDP output, enables HDMI output.

	lock - basic i3lock-color script/config.

	vol(up,down)(10,5) - increments/decrements volume by the listed amount.

	xclipclear - Clears the X clipboard.

	xprint - maim -s piped to the clipboaard. Gets the selected portion, screenshots it, and copies to clipboard.

	xshiftprint - Same as xprint but without the -s tag.

	xservkill - Self explanatory, kills the X server.

	toggleblur - Toggles background blur in picom

qemu/

	qemulate - Starts qemu as a daemon with 6GB of ram with $1 treated as cdrom to boot from, generally a .ISO file, running on a spice server, and also starts remote-viewer listening on that spice socket.

	qemurun - Similar to qemulate but $1 is treated as the virtual hard drive to boot from.

	qemuresume - Basically an aliass for running remote viewer listening on the qemu spice socket.

